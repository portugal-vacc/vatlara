import { SvelteKitAuth } from '@auth/sveltekit';
import type { Handle } from '@sveltejs/kit';
import { env } from '$env/dynamic/private';
import type { OAuth2Config } from '@auth/core/providers';

export const handle = SvelteKitAuth(async (event) => {
	const authOptions = {
		providers: [
			{
				id: 'vatsim',
				name: 'VATSIM SSO',
				type: 'oauth',
				authorization: {
					url: 'https://auth-dev.vatsim.net/oauth/authorize',
					params: { scope: 'full_name vatsim_details email country' }
				},
				token: {
					url: 'https://auth-dev.vatsim.net/oauth/token',
					params: { grant_type: 'authorization_code' }
				},
				userinfo: {
					url: 'https://auth-dev.vatsim.net/api/user'
				},
				profile(profile) {
					return {
						id: profile.data.cid,
						name: profile.data.personal.name_full,
						email: profile.data.personal.email,
						image: profile.data.cid
					};
				},
				clientId: env.VATSIM_ID,
				clientSecret: env.VATSIM_SECRET
			} satisfies OAuth2Config
		],
		secret: env.AUTH_SECRET,
		trustHost: true
	};
	return authOptions;
}) satisfies Handle;
