/** @type {import('./$types').PageServerLoad} */
export async function load({ params }) {
	const countries_availble_response = await fetch(`https://lara.vatsim.pt/api/areas/countries/`);
	return {
		countries_availble: await countries_availble_response.json()
	};
}
